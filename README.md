# spring-boot-lambda

## Run integration tests in AWS
1. From the terraform directory run `terraform init`
1. From the terraform directory run `terraform apply`
1. From the terraform directory save the API gateway url in a bash variable `BASE_URL=$(terraform output base_url)`
1. From the project directory run `./gradlew -DBASE_URL=$BASE_URL clean integrationTest`
1. From the terraform directory run `terraform destroy`

### References
- https://github.com/awslabs/aws-serverless-java-container
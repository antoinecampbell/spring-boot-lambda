provider "aws" {
  region = "us-east-1"
}

locals {
  lambda_zip = "../build/distributions/app.zip"
}

resource "aws_s3_bucket" "terraform_bucket" {
  bucket = "antoine-test-terraform-bucket"
  acl    = "private"
}

resource "aws_s3_bucket_object" "spring_boot_lambda_zip" {
  bucket = "${aws_s3_bucket.terraform_bucket.id}"
  key    = "spring-boot-lambda.zip"
  source = "${local.lambda_zip}"
  etag   = "${md5(file(local.lambda_zip))}"
}

resource "aws_lambda_function" "spring_boot_lambda" {
  function_name = "spring-boot-lambda"
  s3_bucket     = "${aws_s3_bucket.terraform_bucket.id}"
  s3_key        = "${aws_s3_bucket_object.spring_boot_lambda_zip.id}"
  handler       = "com.antoinecampbell.cloud.springbootlambda.StreamLambdaHandler::handleRequest"
  runtime       = "java8"
  timeout       = 120
  memory_size   = 768
  environment {
    variables {
      SPRING_DATASOURCE_URL      = "jdbc:postgresql://${aws_db_instance.db.endpoint}/item"
      SPRING_PROFILES_ACTIVE     = "lambda"
      SPRING_DATASOURCE_USERNAME = "${aws_db_instance.db.username}"
      SPRING_DATASOURCE_PASSWORD = "${aws_db_instance.db.password}"
    }
  }

  vpc_config {
    security_group_ids = ["sg-e96463ab"]
    subnet_ids         = [
      "subnet-18066436",
      "subnet-39bd1607",
      "subnet-4b355517",
      "subnet-52154c5d",
      "subnet-57dfbb30",
      "subnet-cd516687"
    ]
  }
  role = "${aws_iam_role.lambda_exec.arn}"
}

# Create the API gateway
resource "aws_api_gateway_rest_api" "api" {
  name        = "spring-boot-lambda-api-gateway"
  description = "Spring Boot Lmabda API Gateway"
}

# Create proxy resource
resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  parent_id   = "${aws_api_gateway_rest_api.api.root_resource_id}"
  path_part   = "{proxy+}"
}

# Create proxy resource method
resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = "${aws_api_gateway_rest_api.api.id}"
  resource_id   = "${aws_api_gateway_resource.proxy.id}"
  http_method   = "ANY"
  authorization = "NONE"
}

# Create additional integration request for proxy, required when using {proxy+} at the root
resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id = "${aws_api_gateway_method.proxy.resource_id}"
  http_method = "${aws_api_gateway_method.proxy.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.spring_boot_lambda.invoke_arn}"
}

# Create integration request
resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id = "${aws_api_gateway_method.proxy.resource_id}"
  http_method = "${aws_api_gateway_method.proxy.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.spring_boot_lambda.invoke_arn}"
}

# Create deployment
resource "aws_api_gateway_deployment" "test" {
  depends_on = [
    "aws_api_gateway_integration.lambda",
    "aws_api_gateway_integration.lambda_root",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "test"
}

# Allow aPI gateway to invoke function
resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.spring_boot_lambda.arn}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

resource "aws_db_instance" "db" {
  //  identifier          = "test-db"
  allocated_storage   = 20
  storage_type        = "gp2"
  engine              = "postgres"
  engine_version      = "10.6"
  instance_class      = "db.t2.micro"
  name                = "item"
  username            = "item"
  password            = "password"
  skip_final_snapshot = true
  //  parameter_group_name = "db.postgres"
}

data "aws_iam_policy_document" "lambda_vpc_logs" {
  // TODO: Same as arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
  statement {
    effect    = "Allow"
    actions   = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda_policy" {
  name   = "lambda-policy"
  path   = "/"
  policy = "${data.aws_iam_policy_document.lambda_vpc_logs.json}"
}

resource "aws_iam_role_policy_attachment" "lambda" {
  role       = "${aws_iam_role.lambda_exec.name}"
  policy_arn = "${aws_iam_policy.lambda_policy.arn}"
}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "lambda_exec" {
  name = "lambda_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

output "db_username" {
  value = "${aws_db_instance.db.username}"
}

output "db_password" {
  value = "${aws_db_instance.db.password}"
}

output "db_endpoint" {
  value = "${aws_db_instance.db.endpoint}"
}

# URL used to invoke the API gateway
output "base_url" {
  value = "${aws_api_gateway_deployment.test.invoke_url}"
}

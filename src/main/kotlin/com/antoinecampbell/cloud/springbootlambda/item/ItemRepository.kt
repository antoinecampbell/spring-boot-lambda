package com.antoinecampbell.cloud.springbootlambda.item

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface ItemRepository : PagingAndSortingRepository<Item, Long>
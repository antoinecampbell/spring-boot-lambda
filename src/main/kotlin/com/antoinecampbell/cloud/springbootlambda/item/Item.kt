package com.antoinecampbell.cloud.springbootlambda.item

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "item", schema = "public")
class Item(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Long? = null,

        @Column(name = "name", nullable = false, length = 50)
        var name: String? = null,

        @Column(name = "description", length = 255)
        var description: String? = null)
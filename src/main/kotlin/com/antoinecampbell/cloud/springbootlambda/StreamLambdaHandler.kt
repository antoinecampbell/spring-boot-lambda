package com.antoinecampbell.cloud.springbootlambda

import com.amazonaws.serverless.exceptions.ContainerInitializationException
import com.amazonaws.serverless.proxy.model.AwsProxyRequest
import com.amazonaws.serverless.proxy.model.AwsProxyResponse
import com.amazonaws.serverless.proxy.spring.SpringBootLambdaContainerHandler
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import java.io.InputStream
import java.io.OutputStream


@Suppress("unused")
class StreamLambdaHandler : RequestStreamHandler {


    companion object {
        private var handler: SpringBootLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse>

        init {
            try {
                handler = SpringBootLambdaContainerHandler.getAwsProxyHandler(SpringBootLambdaApplication::class.java)
            } catch (e: ContainerInitializationException) {
                // if we fail here. We re-throw the exception to force another cold start
                throw RuntimeException("Could not initialize Spring Boot application", e)
            }

        }
    }

    override fun handleRequest(inputStream: InputStream, outputStream: OutputStream, context: Context) {
        handler.proxyStream(inputStream, outputStream, context)
    }

}
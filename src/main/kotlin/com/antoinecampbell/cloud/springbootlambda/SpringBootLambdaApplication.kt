package com.antoinecampbell.cloud.springbootlambda

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class SpringBootLambdaApplication: SpringBootServletInitializer()

fun main(args: Array<String>) {
	runApplication<SpringBootLambdaApplication>(*args)
}


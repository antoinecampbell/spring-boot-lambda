DROP TABLE IF EXISTS public.item CASCADE;

CREATE TABLE public.item
(
  id          BIGSERIAL   NOT NULL PRIMARY KEY,
  name        VARCHAR(50) NOT NULL,
  description VARCHAR(255)
);
package com.antoinecampbell.cloud.springbootlambda

import com.antoinecampbell.cloud.springbootlambda.item.Item
import io.restassured.RestAssured
import io.restassured.RestAssured.get
import io.restassured.RestAssured.given
import org.hamcrest.Matchers.greaterThanOrEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType

class AwsIntegrationTest {

    @BeforeEach
    internal fun setUp() {
        RestAssured.baseURI =
                System.getProperty("BASE_URL") ?: System.getenv("BASE_URL") ?: "http://localhost:8080"
        println("BASE_URL: ${RestAssured.baseURI}")
    }

    @Test
    internal fun shouldGetItems() {
        get("/items")
                .then()
                .body("_embedded.items.size()", greaterThanOrEqualTo(2))
    }

    @Test
    internal fun shouldCreateItem() {
        val item = Item(name = "Test")
        given().body(item)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`()
                .post("/items")
                .then()
                .statusCode(201)
    }
}